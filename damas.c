#include <stdio.h>
#include <stdlib.h>

int const T = 8;
int turno = 1;
char tablero[8][8];

void llenarTablero(){
	for(int i = 0; i < T; i++)
	        for(int j = 0; j < T; j++)
		{
			if (i < 3 && ((i + j) % 2 == 0))
				tablero[i][j] = 'R';
			else
				if ((i > 4) && ((i + j) % 2 == 0))
					tablero[i][j] = 'B';
				else
					tablero[i][j] = '-' ;
		}
}

void pausar()
{
	system("clear");
}
void pausar2()
{
	printf("\n precione 'c' para continuar...");
	while(getchar() != 'c');
}

void imprimirTablero(){
	system("clear");
	for(int i = 0; i < T; i++)
    	{
		if (i == 0)
		{
			printf("  ");
			for (int k = 0; k < T; k++)
				printf(" %i ", k);
			printf("\n");
		}

		printf("%i ", i);
        	for(int j = 0; j < T; j++)
        	{
		
                	printf(" %c ", tablero[i][j]);
          	}
          	printf("\n");
    	}
}

void moverPieza(int x, int y, int direccion)
{
	if (!turno)
	{
		if (tablero[x][y] != 'R')
		{
			printf("No hay una pieza en esta posicion \n");
			pausar2();
		}
		else 
		{
			printf("Direccion %i \n", direccion);
			if (direccion == 1 && (y - 1) >= 0 && (x + 1) < 8)
			{
				if (tablero[x + 1][y - 1] == '-')
				{
					tablero[x][y] = '-';
					tablero[x + 1][y - 1] = 'R';
				}
				else
					if(tablero[x + 1][y - 1] == 'B' && tablero[x + 2][y - 2] == '-')
					{
						tablero[x][y] = '-';
						tablero[x + 1][y - 1] = '-';
						tablero[x + 2][y - 2] = 'R';
						printf("La pieza Roja se comio a la Blanca en la coordenada %i %i \n", x + 1, y - 1);
						pausar2();
					}
					else{
						printf("La casilla a donde se quiere mover esta ocupada");
						pausar2();}
			}
			else
			{
				if (direccion == 1){
					printf("La pieza se sale del tablero");
					pausar2();}
			}
			
			if (direccion == 2 && (y + 1) < 8 && (x + 1) < 8)
			{
				if (tablero[x + 1][y + 1] == '-')
				{
					tablero[x][y] = '-';
					tablero[x + 1][y + 1] = 'R';
				}
				else
					if(tablero[x + 1][y + 1] == 'B' && tablero[x + 2][y + 2] == '-')
					{
						tablero[x][y] = '-';
						tablero[x + 1][y + 1] = '-';
						tablero[x + 2][y + 2] = 'R';
						printf("La pieza Roja se comio a la Blanca en la coordenada %i %i \n", x + 1, y + 1);
						pausar2();
					}
					else{
						printf("La casilla a donde se quiere mover esta ocupada");
						pausar2();}
			}
			else
			{
				if (direccion == 2){
					printf("La pieza se sale del tablero");
					pausar2();}
			}
		}
		
	}
	else
	{
		if (tablero[x][y] != 'B')
		{
			printf("No hay piza en estga posicion %i %i \n", x, y);
			pausar2();
		}
		else 
		{
			if (direccion == 1 && (y - 1) >= 0 && (x - 1) >= 0)
			{
				if (tablero[x - 1][y - 1] == '-')
				{
					tablero[x][y] = '-';
					tablero[x - 1][y - 1] = 'B';
				}
				else
					if(tablero[x - 1][y - 1] == 'R' && tablero[x - 2][y - 2] == '-')
					{
						tablero[x][y] = '-';
						tablero[x - 1][y - 1] = '-';
						tablero[x - 2][y - 2] = 'B';
						printf("La pieza Blanca se comio a la Roja en  la coordenada %i %i \n", x - 1, y - 1);
						pausar2();
					}
					else{
						printf("La casilla donde se quiere mover esta ocupada");
						pausar2();}
			}
			else
			{
				if (direccion == 1){
					printf("La pieza se sale del tablero");
					pausar2();}
			}
			
			if (direccion == 2 && (y + 1) < 8 && (x - 1) >= 0)
			{
				if (tablero[x - 1][y + 1] == '-')
				{
					tablero[x][y] = '-';
					tablero[x - 1][y + 1] = 'B';
				}
				else
					if(tablero[x - 1][y + 1] == 'R' && tablero[x - 2][y + 2] == '-')
					{
						tablero[x][y] = '-';
						tablero[x - 1][y + 1] = '-';
						tablero[x - 2][y + 2] = 'B';
						printf("La pieza Blanca se comio a la Roja en la coordenada %i %i \n", x + 1, y + 1);
						pausar2();
					}
					else{
						printf("La casilla a donde se quiere mover esta ocupada");
						pausar2();}
			}
			else
			{
				if (direccion == 2){
					printf("La pieza se sale del tablero");
					pausar2();}
			}
		}
	}
	pausar();
}

int jugar()
{
	int x, y, direccion;
	imprimirTablero();
	if (turno)
	{
		printf("Seleccione la ficha Roja que desea mover...\n");
		turno = 0;
	}
	else
	{
		printf("Seleccione la ficha Blanca que desea mover...\n");
		turno = 1;
	}
	printf("\tIntroduzca la fila: ");
	scanf("%i", &x);
	printf("\tIntroduzca la columna: ");
	scanf("%i", &y);
	printf("\tElija la direccion del movimiento, Izquierda (1) Derecha (2): ");
	scanf("%i", &direccion);
	moverPieza(x, y, direccion);
	return 1;
}


int main(){
	system("clear");
    	llenarTablero();
	while(jugar());
	return 0;
}
